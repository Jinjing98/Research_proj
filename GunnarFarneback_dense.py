 # Importing libraries
import cv2
import math
import numpy as np
# Capturing the video file 0 for videocam else you can provide the url

capture = cv2.VideoCapture(r"C:\Users\ijaradar\Google Drive\files.sem3\research_proj\video64.mp4")  #video64 68

# def sigmoid(x):
#      return 1 / (1 + math.e ** -x)



# Reading the first frame
_, frame1 = capture.read()
# Convert to gray scale
prvs = cv2.cvtColor(frame1, cv2.COLOR_BGR2GRAY)
# Create mask
hsv_mask = np.zeros_like(frame1)
# Make image saturation to a maximum value   HSV 8UC则取值范围是h为0-180、s取值为0-255、v取值是0-255.

hsv_mask[..., 1] = 255






# Till you scan the video
while(1):


	# Capture another frame and convert to gray scale
	_, frame2 = capture.read()
	next = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)

	# Computes a dense optical flow using the Gunnar Farneback's algorithm.
	flow = cv2.calcOpticalFlowFarneback(prvs, next, None, pyr_scale = 0.5, levels = 3, winsize = 5, iterations = 5, poly_n = 5, poly_sigma = 1.1,flags = 0)


	# Compute magnite and angle of 2D vector
	mag, ang = cv2.cartToPolar(flow[..., 0], flow[..., 1],angleInDegrees= False)   #in radians not degrees
	# Set image hue according to the angle of optical flow   HUE  1st d of hsv
	hsv_mask[..., 0] = ang * 180 / np.pi / 2
	# Set value as per the normalized magnitude of optical flow      Value 3rd d of hsv
	hsv_mask[..., 2] = cv2.normalize(mag**2, None, 0, 255, cv2.NORM_MINMAX )   #nolinear transformation of value   sigmoid?
	# var = hsv_mask[..., 2] - mag

	# another way  of normolize maginitude   use mag**2 is better than mag since this way can enlarge large movement and filter out minor movement


	# Convert to rgb
	rgb_representation = cv2.cvtColor(hsv_mask, cv2.COLOR_HSV2BGR)
	double_representation = cv2.addWeighted(rgb_representation,3,frame2, 0.5, 0)


	cv2.imshow('result_window', double_representation)
	# cv2.imshow('result_window', rgb_representation)

	kk = cv2.waitKey(20) & 0xff
	# Press 'e' to exit the video
	if kk == ord('e'):
		break
	# Press 's' to save the video
	elif kk == ord('s'):
		cv2.imwrite('Optical_image.png', frame2)
		cv2.imwrite('HSV_converted_image.png', rgb_representation)
	prvs = next

capture.release()
cv2.destroyAllWindows()




