import numpy as np
import cv2

cap = cv2.VideoCapture(r"C:\Users\ijaradar\Google Drive\files.sem3\research_proj\video01.mp4")


# params for corner detection
feature_params =  dict(maxCorners = 100,
					 qualityLevel = 0.3,
					 minDistance = 5,
					blockSize = 10 )

# Parameters for lucas kanade optical flow
lk_params = dict( winSize = (15, 15),
				 maxLevel = 2,
				criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT,
							10, 0.03))

# Create some random colors
color = np.random.randint(0, 255, (100, 3))

# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv2.cvtColor(old_frame,
						cv2.COLOR_BGR2GRAY)
p0 = cv2.goodFeaturesToTrack(old_gray, mask = None,
							**feature_params)

# Create a mask image for drawing purposes
mask = np.zeros_like(old_frame)

while(1):

	ret, frame = cap.read()
	frame_gray = cv2.cvtColor(frame,
							cv2.COLOR_BGR2GRAY)

	# calculate optical flow
	p1, st, err = cv2.calcOpticalFlowPyrLK(old_gray,
										frame_gray,
										p0, None,
										**lk_params)

	# Select good points   problem   at the beginning if not traced some points then it will never pop up   eg scissor
	good_new = p1[st == 1]
	good_old = p0[st == 1]

	# draw the tracks
	for i, (new, old) in enumerate(zip(good_new,
									good_old)):
		a, b = new.ravel()
		c, d = old.ravel()
		mask = cv2.line(mask, (a, b), (c, d),
						color[1].tolist(), 2)

		frame = cv2.circle(frame, (a, b), 5,
						color[1].tolist(), -1)

	img = cv2.add(frame, mask)

	cv2.imshow('frame', img)


	kk = cv2.waitKey(20) & 0xff
	# Press 'e' to exit the video
	if kk == ord('e'):
		break
	# Updating Previous frame and points
	old_gray = frame_gray.copy()
	p0 = good_new.reshape(-1, 1, 2)
	# p0 = cv2.goodFeaturesToTrack(old_gray, mask = None, **feature_params)


cv2.destroyAllWindows()
cap.release()
